	// Try BCD function in x87 FPU
	// 2012 H.Tomari. Public Domain.
	// This file works in AMD64 environments
	.text
	.globl	tobcd
	.type	tobcd, @function
tobcd:
	movsd	%xmm0, -8(%rsp)
	fldl	-8(%rsp)
	fbstp	(%rdi)
	ret
