// Try Crazy BCD function in x87 FPU
// 2012 H.Tomari. Public Domain.
// build>> g++ -Wall -o fpubcd -O3 fpubcddrv.cc fpubcd.s
#include <iostream>
using namespace std;
const char *digits="0123456789ABCDEF";
const signed int imin=-50000;
const signed int imax=50000;
const signed int step=12437;
extern "C" {
extern void tobcd(unsigned char *,double);
}
static void printbcd(unsigned char *);
static void printbcd(unsigned char *bcd) {
	unsigned int i;
	unsigned int f=0;
	if(bcd[9]&0xf0) cout << '-';
	for(i=1; i<20; i++) {
		unsigned char o;
		char d=digits[o=(bcd[(19-i)>>1]>>((i&1)?0:4))&0xf];
		f+=o;
		if(f) cout << d;
	}
	cout<<endl;
}
int main(int argc, char *argv[]) {
	unsigned char bcdbuf[10];
	signed int i;
	double num;
	cout << "LIBC++, FPU" << endl;
	for(i=imin; i<=imax; i+=step) {
		num=(double) i;
		tobcd(bcdbuf,num);
		cout <<i << ", ";
		printbcd(bcdbuf);
	}
	return 0;
}
